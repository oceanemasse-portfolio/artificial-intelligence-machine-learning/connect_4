"""
MASSE Océane
Class Game
"""

################ IMPORTS
from __future__ import (
    annotations,
)  #  corrige pblm de typing qui ne peut pas se référer à la classe actuelle

from Player import *
from Board import *
from BoardTree import *


import numpy as np
from typing import *
import copy
import time
import random

import pandas

################# CLASS GAME

class Game:
    """
    Classe définissant ce qui est relié aux "règles" d'une partie ( ajouter un pion, afficher, ... )
    """

    def __init__(self, player1: Player, player2: Player):
        self.board = Board(grid=np.zeros((6, 7)))
        self.player1 = player1
        self.player2 = player2

    def add_pawn(self, position: Tuple[int, int], player: Player):
        """
        permet d'ajouter un pion dans la grille

        :param position: coordonnées où ajouter le pion
        :param player: Joueur pour lequel ajouter le pion
        """
        self.board.grid[position[0]][position[1]] = player.colour

    def display(self):
        """
        Affiche le plateau avec la grille et les coordonnées pour plus de lisibilité
        """
        print(pandas.DataFrame(self.board.grid, index=[0, 1, 2, 3, 4, 5], columns=[0, 1, 2, 3, 4, 5, 6], dtype=int))

    def best_alignement_move(self, player: Player) -> Tuple:
        """
        donne le coup pemettant le meilleur alignement de pions selon les coups disponibles
        
        :param player: joueur pour qui regarder les coups

        :return: coordonnées du coup
        """
        for alignement_nbr in range(4, 0, -1):
            for move in self.board.generate_moves():
                board = self.board.create_board_with_next_move(move, player)
                for row in board.grid:
                    if alignement_nbr in self.board.adjacent_pawns_count(row, player):
                        return move
                for column in np.transpose(board.grid):
                    if alignement_nbr in self.board.adjacent_pawns_count(column, player):
                        return move
                for diag_left in board.board_get_diags(direction="left"):
                    if alignement_nbr in self.board.adjacent_pawns_count(diag_left, player):
                        return move
                for diag_right in board.board_get_diags(direction="right"):
                    if alignement_nbr in self.board.adjacent_pawns_count(diag_right, player):
                        return move

    def did_player_won(self, player: Player):
        """
        établie si le joueur après son coup vient de gagner ou égaliser
        Vérifier que le score vaut 1 équivaut à vérifer si le joueur vient
        d'aligner 4 pions ( et donc gagner)

        :param player: Joueur pour lequel vérifier
        """
        for row in self.board.grid:
            if self.board.score_cal_vector(row, player) >= 1555:
                return True
        for column in np.transpose(self.board.grid):
            if self.board.score_cal_vector(column, player) >= 1555:
                return True
        for diag in self.board.board_get_diags(direction="left"):
            if self.board.score_cal_vector(diag, player) >= 1555:
                return True
        for diag in self.board.board_get_diags(direction="right"):
            if self.board.score_cal_vector(diag, player) >= 1555:
                return True

    def play(self, player_who_plays: Player, player1: Player, player2: Player, strategy = "random"):
        """
        Génère les coups possibles et joue le meilleur selon la stratégie définie
        random génère les coups aléatoirement
        heuristics génère les coups pour l'heuristique maximal
        max_score priorise le score devant l'alignement
        aligned priorise le fait d'aligner ses pions


        :param player: joueur qui joue
        :param strategy: choisis la stratégie entre random, heuristic et aligned
        """
        assert strategy == "random" or strategy == "heuristic" or strategy == "max_score" or strategy == "aligned" or strategy == "minmax" or strategy == "human"

        if strategy == "random":
            random_move = random.choice(self.board.generate_moves())
            self.add_pawn(random_move, player_who_plays)

        elif strategy == "aligned":
            self.add_pawn(self.best_alignement_move(player_who_plays), player_who_plays)

        elif strategy == "max_score":
            best_move = tuple()
            score_max = 0
            for move in self.board.generate_moves():
                board = self.board.create_board_with_next_move(move, player_who_plays)
                score = board.board_final_score(player_who_plays, player1, player2)
                if score > score_max:
                    score_max = score
                    best_move = move
            self.add_pawn(best_move, player_who_plays)

        elif strategy == "heuristic":
            best_move = tuple()
            heuristique_max = 0
            heuristique = self.board.heuristique_board()
            for move in self.board.generate_moves():
                heuristique_move = heuristique[move]
                if heuristique_move > heuristique_max:
                    heuristique_max = heuristique_move
                    best_move = move
            self.add_pawn(best_move, player_who_plays)

        elif strategy == "minmax":
            depth = 0
            if player_who_plays == player1:
                depth = 5
            elif player_who_plays == player2:
                depth = 4

            self.board = Board(BoardTree(self.board, depth, player1, player2).minmax(player_who_plays)[1])

        elif strategy == "human":
            move = input("Entrez les coordonnées du coup sous forme de tuple(int, int) ou écrivez exit() pour quitter:\n")

            while type(eval(move)) != tuple:
                move = input("Type non valide. Entrez les coordonnées du coup sous forme de tuple ou écrivez exit() pour quitter:\n")
            while len(eval(move)) != 2:
                move = input("Coordonnées incorrectes. Entrez les coordonnées du coup sous forme de tuple ou écrivez exit() pour quitter:\n")
            while self.board.is_move_legit(eval(move)) != True:
                move = input("Coup impossible. Entrez les coordonnées du coup sous forme de tuple ou écrivez exit() pour quitter:\n")

            self.board.add_pawn(eval(move), player_who_plays)