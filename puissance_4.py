"""
Projet Puissance 4
MASSE Océane

C'est dans ce fichier que se déroule le jeu du puissance 4.

"""

################################ IMPORTS
from __future__ import (
    annotations,
)  #  corrige pblm de typing qui ne peut pas se référer à la classe actuelle

from typing import *

from Player import *
from Board import *
from Game import *
from BoardTree import *

################################# FONCTIONS

def player_turn(player_who_plays: Player, player1: Player, player2: Player, strat: str):
    """
    Les étapes du tour d'un joueur. Chaque joueur joue selon une sratégie
    Si la stratégie est human alors ce n'est pas le l'ia qui joue.
    Les stratégies d'IA possibles sont: random, aligned, heuristic, max_score et minmax
    (fonctionnement dans la class Game et fonction play)

    :param player_who_plays: joueur qui doit jouer
    :param player1: joueur1
    :param player2: joueur2
    :param strat: stratégie du joueur à appliquer
    """
    assert strat == "random" or strat == "aligned" or strat == "heuristic" or strat == "max_score" or strat == "minmax" or strat == "human"

    if player_who_plays == player1:
        print("\nTour de player1, décision du prochain coup...\n")
    else:
        print("\nTour de player2, décision du prochain coup...\n")

    game.play(player_who_plays, player1, player2, strategy=strat)
    game.display()

    if game.did_player_won(player_who_plays) == "draw":
        print("DRAW")
        exit()
    if game.did_player_won(player_who_plays) == True:
        print(player_who_plays.name, "WON")
        exit()

def play_a_game(player1: Player, player2: Player, strat_p1: str, strat_p2: str):
    """
    Alternance des tours des deux joueurs 
    Permet de lancer une partie
    """
    while True:

        player_turn(player1, player1, player2, strat_p1)

        time.sleep(1)

        player_turn(player2, player1, player2, strat_p2)

################################# JEU
"""
Rappel des stratégies possibles : random, max_score, aligned, heuristic, minmax et human ( lorsque vous souhaitez jouer)
"""

# Assignation
# Changez les noms comme vous le souhaitez (Player(nom, couleur))
player1 = Player("IA1 random", "yellow")
player2 = Player("IA2 minmax", "red")
game = Game(player1, player2)


#Partie

print("\nJ1 joue avec random et J2 avec minmax\n")
game.display()
play_a_game(player1, player2, "random", "minmax")

#_______ Autre essai

# Assignation
"""
player1 = Player("HUMAIN", "yellow")
player2 = Player("IA2 minmax", "red")
game = Game(player1, player2)

print("\nVous etes J1 et jouez contre minmax")
game.display()
play_a_game(player1, player2, "human", "minmax")
"""