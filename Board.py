"""
MASSE Océane
Class Board
"""

###################### IMPORTS

from __future__ import (
    annotations,
)  #  corrige pblm de typing qui ne peut pas se référer à la classe actuelle

from Player import *


import numpy as np
from typing import *
import copy

###################### CLASS BOARD

class Board:
    """
    Classe définissant ce qu'est le plateau de jeu et les actions réalisables dessus
    Le plateau est composé d'une grille d'une certaine dimension par un numpy.array
    """

    def __init__(self, grid: np.array):
        self.grid = grid

    def is_move_legit(self, move: Tuple[int, int]) -> bool:
        """
        Vérifie si le coup est possible physiquement, 
        c'est à dire qu'il n'y a pas de pion à cet endroit
        et qu'il y a un pion sur lequel etre posé si ce n'est pas la 1ere ligne

        :param move: coordonnées du coup 

        :return: True si le coup est possible sinon False
        """
        board_last_row = self.grid.shape[0] - 1
        if move[0] == board_last_row:
            return self.grid[move] == 0
        elif self.grid[move] == 0:
            column = self.grid[:, move[1]]
            return column[move[0] + 1] != 0 # regarde si un pion est en dessous
        return False

    def generate_moves(self) -> List[Tuple[int, int]]:
        """
        Génère la liste des coups possibles par rapport à la grille de jeu

        :return: liste des coups possibles
        """
        moves_list = []
        for row in range(self.grid.shape[0]):
            for column in range(self.grid.shape[1]):
                moves_list.append((row, column))
        return [move for move in moves_list if self.is_move_legit(move)]

    def add_pawn(self, position: Tuple[int, int], player: Player):
        """
        Permet d'ajouter un pion à la grille de jeu

        :param position: Coordonnées où ajouter le pion
        :param player: Joueur pour qui réaliser le coup
        """
        self.grid[position[0]][position[1]] = player.colour

    def create_board_with_next_move(
        self, move: Tuple[int, int], player: Player
    ) -> Board:
        """
        Créer un plateau avec la grille contenant le nouveau coup

        :param move: coordonnées du coup
        :param player: Joueur pour qui réaliser le coup

        :return: Plateau avec la grille et le nouveau coup
        """
        grid = copy.deepcopy(self.grid)
        grid[move[0]][move[1]] = player.colour
        return Board(grid)

    def adjacent_pawns_count(self, vector: np.array, player: Player) -> List[int]:
        """
        Permet d'avoir la liste des nombres de memes pions alignés

        :param vector: Vecteur pour lequel regarder les pions alignés
        :param player: Joueur pour lequel regarder les pions alignés

        :return: liste des nombres de pions alignés
        """
        pawns_count = []
        cur_pawn_count = 0
        in_sequence = False

        for pawn in vector:

            if pawn == player.colour:
                in_sequence = True

            elif in_sequence:
                in_sequence = False
                pawns_count.append(cur_pawn_count)
                cur_pawn_count = 0

            if in_sequence:
                cur_pawn_count += 1

        if in_sequence:
            pawns_count.append(cur_pawn_count)

        return pawns_count

    def board_get_diags(self, direction="left") -> List[np.array]:
        """
        Pemert d'avoir une liste de toutes les diagonales de la grille
        dans un certains sens :
        Les diags sont soit orientées de droite vers la gauche, 
        soit de la gauche vers la droite
        (pensez à une croix)

        :param direction: choisis l'orientation des diagonales
        left pour droite vers gauche
        right pour gauche vers droite

        :return: liste des diags dans le sens voulu
        """
        assert direction == "left" or direction == "right"

        board = self.grid if direction == "left" else np.fliplr(self.grid)
        #fliplr inverse la matrice dans la direction gauche droite 
        #utile pou calculer l'autre sens des diagonales
        diags = []

        d = 0 # diagonale du centre
        # On récupère ici les diagonales du haut de la matrice
        while True:
            diag = np.diag(board, k=d) 
            if len(diag) == 0: 
                break
            diags.append(diag)
            d += 1 # les diagonales vers le haut

        d = -1 #diagonale sous le centre
        while True:
            diag = np.diag(board, k=d)
            if len(diag) == 0:
                break
            diags.append(diag)
            d -= 1 # les diagonales vers le bas

        return diags

    def heuristique_board(self) -> np.array:
        """
        Le plateau heuristique représente le nombre d'alignements de 4 pions possibles
        pour chaque emplacement

        :return: tableau heuristique
        """
        return np.array(
        [[3, 4, 5, 7, 5, 4, 3],
        [4, 6, 8, 10, 8, 6, 4], 
        [5, 8, 11, 13, 11, 8, 5], 
        [5, 8, 11, 13, 11, 8, 5],
        [4, 6, 8, 10, 8, 6, 4 ],
        [3, 4, 5, 7, 5, 4, 3]])   

    def score_cal_vector(self, vector: np.array, player_who_plays: Player) -> float:
        """
        Calcule le score maximal de mon vecteur
        L'alignement d'un pion vaut 1. Chaque joueur peut placer 21 pions, 
        le score d'alignement total de 1 pion (21*1) ne peut pas valoir plus qu'un alignement
        de 2 pions. Donc un alignement de 2 pions vaut 22.
        Suivant le meme raisonnement, un alignement de 3 pions vaut 222 et un de 4
        1555 de score.

        :param vector: Vecteur qui représente soit une ligne, colonne ou diagonale
        :param player: Joueur pour lequel calculer le score

        :return: score du joueur
        """
        adjacent_pawns_count_player = self.adjacent_pawns_count(vector, player_who_plays)

        if len(adjacent_pawns_count_player) == 0:
            return 0
        score = 0
        for adjacent_pawn in adjacent_pawns_count_player:
            if adjacent_pawn == 1:
                score += 1
            elif adjacent_pawn == 2:
                score += 22
            elif adjacent_pawn == 3:
                score += 222
            elif adjacent_pawn == 4:
                score += 1555
        return score

    def board_score_row_column(self, player_who_plays: Player, transpose=False) -> float:
        """
        Calcul le score final de toutes les lignes OU toutes les colonnes de la 
        grille de jeu
        La transposée permet d'inverser les lignes et les colonnes

        :param player: Joueur pour lequel calculer le score
        :param transpose: Calcule pour les lignes si False sinon pour les colonnes

        :return: score final du joueur pour les lignes OU les colonnes
        """
        board = np.transpose(self.grid) if transpose else self.grid
        final_score = 0
        for row in board:
            final_score += self.score_cal_vector(row, player_who_plays)
        return final_score


    def board_score_diag(self, player_who_plays: Player, direction="left") -> float:
        """
        Calcule le score max des diagonales selon le sens donné
        Les diags sont soit orientées de droite vers la gauche, 
        soit de la gauche vers la droite

        :param player: Joueur pour lequel compter le score
        :param direction: choisis l'orientation des diagonales
        left pour droite vers gauche
        right pour gauche vers droite 

        :return: score final des diagonales pour un sens donné
        """
        assert direction == "left" or direction == "right"
        return sum(
            [
                self.score_cal_vector(diag, player_who_plays)
                for diag in self.board_get_diags(direction)
            ]
        )

    def board_final_score(self, player_who_plays: Player, player1, player2) -> float:
        """
        Il s'agit de la fonction d'évaluation (celle qui calcul le score final)
        Permet de faire la synthèse du score général de cette grille pour
        un joueur, selon tous les axes
        Elle attribut un malus très négatif si au tour suivant l'adversaire a la
        possibilité de gagner.

        :param player: Joueur pour lequel calculer le score
        :return: score du board
        """
        score = (
            self.board_score_row_column(player_who_plays, transpose=False)
            + self.board_score_row_column(player_who_plays, transpose=True)
            + self.board_score_diag(player_who_plays, direction="left")
            + self.board_score_diag(player_who_plays, direction="right")
        )


        if player_who_plays == player1:
            opponent = player2
        else:
            opponent = player1

        for move in self.generate_moves():
            board = self.create_board_with_next_move(move, opponent)
            score_opponent = (
                board.board_score_row_column(opponent, transpose=False)
                + board.board_score_row_column(opponent, transpose=True)
                + board.board_score_diag(opponent, direction="left")
                + board.board_score_diag(opponent, direction="right")
            )
            if score_opponent >= 1555: # si l'adversaire gagne 
                score += -10000 # grand malus dans le score de ce board
                
        return score